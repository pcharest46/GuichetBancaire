#include "guichet.h"

void main()
{
	setlocale(LC_ALL, "");
	Liste* maliste = new Liste;
	compte* ptrCompte;

	ptrCompte = NULL;
	maliste->premier = NULL;
	maliste->dernier = NULL;
	maliste->total = 0;

	PresentationInitiale();
	AfficherTitre("banque royale");
	AfficherTitre("guichet automatique bancaire");

	LectureFichierTxt(maliste, ptrCompte);

	//Fonction pour Valider Identifiants & Retourner le compte identifi�

	ptrCompte = ValiderIdentifiantsRetournerCompte(maliste);
	MenuPrincipal(maliste, ptrCompte);

	EcritureFichierTxt(maliste, ptrCompte);

	system("pause");
}

void PresentationInitiale()
{
	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl;
	cout << "\t\t#########################################" << endl;
	cout << "\t\t#                BANQUE ROYALE          #" << endl;
	cout << "\t\t#########################################" << endl;
	system("pause > nul");
	system("CLS");
}

void AfficherTitre(char* ptrTitre)
{
	int i = 0;
	cout << "\t\t";
	while (*ptrTitre != '\0')
	{
		cout << (char)toupper(*ptrTitre);
		ptrTitre++;
		i++;
	}
	cout << "\n\t\t";

	for (int j = 0; j < i; j++)
	{
		cout << "-";
	}
	cout << "\n\n";
}
compte* ValiderIdentifiantsRetournerCompte(Liste* uneliste)
{
	int i = 0;
	compte* temp = uneliste->premier;
	string numero;
	string nip;
	bool access = false;

	// Verification no de compte

	do {
		temp = uneliste->premier;
		cout << "Entrez votre num�ro de compte:\t";
		cin >> numero;

		while (temp != NULL)
		{
			if (numero == temp->numero)
			{
				access = true;
				break;
			}
			temp = temp->suivant;
			i++;

			if (uneliste->total == i)
			{
				cout << "Num�ro de compte invalide.";
				system("pause > nul");
				system("CLS");
				access = false;
			}
		}

	} while (access == false);

	// Verification du nip

	cout << "\nBienvenue " << temp->prenom << " " << temp->nom << endl;
	do {
		cout << "\nEntrez votre nip:\t";
		cin >> nip;

		if (nip == temp->nip)
		{
			access = true;
		}
		else
		{
			cout << "Num�ro de nip invalide.";
			system("pause > nul");
			access = false;
		}
	} while (access == false);

	return temp;
}

void MenuPrincipal(Liste* uneliste, compte* tmp)
{
	int choix = 0;

	do {

		cout << "\nVeuillez choisir la transaction d�sir�\n1-D�p�t\n2-Retrait\n3-Consulter Infos Compte\n4-Quitter\n\n";
		cin >> choix;

		switch (choix)
		{
		case 1:
			Depot(uneliste, tmp);
			system("CLS");
			Consultation(uneliste, tmp);
			system("CLS");
			MenuPrincipal(uneliste, tmp);
			break;

		case 2:
			Retrait(uneliste, tmp);
			system("CLS");
			Consultation(uneliste, tmp);
			system("CLS");
			MenuPrincipal(uneliste, tmp);
			break;

		case 3:
			Consultation(uneliste, tmp);
			system("CLS");
			MenuPrincipal(uneliste, tmp);
			break;
		case 4:
			cout << "Merci d'avoir utilis� nos service.\n";
			break;
		default:
			cout << "Veuillez choisir une option entre 1 et 4.";
			system("CLS");
			MenuPrincipal(uneliste, tmp);
		}

	} while (choix < 1 || choix>4);
}

void Retrait(Liste* uneliste, compte* tmp)
{
	float montant = 0;
	do {
		cout << "Veuillez entrer le montant que vous d�sirez retirer du compte:\t";
		cin >> (float)montant;
		// Si le nombre est divisible par 20. Nombre doit etre entre 20 et 500. Doit etre plus petit que le solde (3) conditions
		if (fmod(montant, 20) != 0 || montant < 20 || montant>500 || tmp->solde < montant)
		{
			cout << "Montant invalide\n";
			if (fmod(montant, 20) != 0)
				cout << "Raison: Montant doit �tre un multiple de 20.\n";
			if (montant < 20 || montant>500)
				cout << "Raison: Montant doit �tre situ� entre 20 et 500$.\n";
			if (tmp->solde < montant)
				cout << "Raison: Veuillez vous assurer que le montant ne d�passe pas votre solde actuel.\n";
		}
	} while (fmod(montant, 20) != 0 || montant < 20 || montant>500 || tmp->solde < montant);

	tmp->solde -= montant;
}
void Depot(Liste* uneliste, compte* tmp)
{
	float montant = 0;
	do {
		cout << "Veuillez entrer le montant que vous d�sirez d�poser:\t";
		cin >> (float)montant;
		if (montant < 2 || montant >20000)
			cout << "Svp entrez un montant entre 2$ et 20 000$.";
	} while (montant<2 || montant >20000);
	tmp->solde += montant;
}
void Consultation(Liste* uneliste, compte* tmp)
{
	cout << setiosflags(ios::left) << setw(10) << "Numero" << setw(10) << "Prenom" << setw(10) << "Nom" << setw(5) << "Nip" << setw(5) << "Solde\n\n";
	cout << setw(10) << tmp->numero << setw(10) << tmp->prenom << setw(10) << tmp->nom << setw(5) << tmp->nip << setw(5) << tmp->solde << endl;
	system("pause > nul");
}
void LectureFichierTxt(Liste* uneliste, compte* tmp)
{
	//Lecture fichier

	// cr�ation du fichier
	ifstream readmyfile("comptes.txt", ios::in);
	// message si la cr�ation du fichier a �chou�e
	if (!readmyfile)
	{
		cerr << "Ouverture de fichier impossible";
		exit(1);
	}
	tmp = new compte;
	// on entre les valeurs dans le fichier

	while (readmyfile >> tmp->numero >> tmp->prenom >> tmp->nom >> tmp->nip >> tmp->solde)
	{

		if (uneliste->total == 0)
		{
			tmp->suivant = NULL;
			uneliste->premier = tmp;
			uneliste->dernier = tmp;
		}
		else
		{
			tmp->suivant = NULL;
			uneliste->dernier->suivant = tmp;
			uneliste->dernier = tmp;
		}
		tmp = new compte;
		uneliste->total += 1;
	}
	readmyfile.close();
}
void EcritureFichierTxt(Liste* uneliste, compte* tmp)
{
	// Ecriture dans le fichier

	ofstream writemyfile("comptes.txt", ios::out);

	//cout << setiosflags(ios::left) << setw(10) << "Numero" << setw(10) << "Prenom" << setw(10) << "Nom" << setw(5) << "Nip" << setw(5) << "Solde\n\n";

	tmp = uneliste->premier;

	while (tmp != NULL)
	{
		writemyfile << tmp->numero << " " << tmp->prenom << " " << tmp->nom << " " << tmp->nip << " " << tmp->solde << "\n";
		tmp = tmp->suivant;
	}
}