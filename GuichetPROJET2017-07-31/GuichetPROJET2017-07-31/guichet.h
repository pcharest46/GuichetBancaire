#pragma once
#include <iostream> // input output stream
#include <string> // format text
#include <fstream> // to read/write in a txt file
#include <iomanip> // format width
using namespace std;

struct compte
{
	string numero;
	string prenom;
	string nom;
	string nip;
	float solde;
	compte* suivant;
};

struct Liste
{
	compte* premier;
	compte* dernier;
	int total;
};

// Déclaration des prototypes de nos fonctions
void PresentationInitiale();
void AfficherTitre(char*);
compte* ValiderIdentifiantsRetournerCompte(Liste*);
void MenuPrincipal(Liste*, compte*);
void Retrait(Liste*, compte*);
void Depot(Liste*, compte*);
void Consultation(Liste*, compte*);
void LectureFichierTxt(Liste*, compte*);
void EcritureFichierTxt(Liste*, compte*);